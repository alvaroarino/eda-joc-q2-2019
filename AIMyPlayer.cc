#include "Player.hh"


/**
 * Write the name of your player and save this file
 * with the same name and .cc extension.
 */
#define PLAYER_NAME Andy


struct PLAYER_NAME : public Player {
    
    /**
     * Factory: returns a new instance of this class.
     * Do not modify this function.
     */
    static Player* factory () {
        return new PLAYER_NAME;
    }
    
    /**
     * Types and attributes for your player can be defined here.
     */
    
    bool theAntIsMine(int id, Pos p, vector<pair<int, Pos>>& posiciones) {
        if (round() == 0) return true;
        vector<Dir> direccions = {Up, Right, Down, Left};
        for (int i = 0; i < posiciones.size(); ++i) {
            for (int j = 0; j < direccions.size(); ++j) {
                if (pos_ok(p) and (posiciones[i].first == id and posiciones[i].second == p)) {
                    return true;
                } else {
                    Pos ps = p + direccions[j];
                    if (pos_ok(ps) and (posiciones[i].first == id and posiciones[i].second == ps)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    void updatePosition(int id, Pos p, vector<pair<int, Pos>>& posiciones) {
        bool modified = false;
        for (int i = 0; i < posiciones.size() and not modified; ++i) {
            if (id == posiciones[i].first) {
                posiciones[i].second = p;
            }
        }
        if (not modified) {
            posiciones.push_back(make_pair(id, p))
        }
    }
    
    bool hasReserves(int id, AntType type) {
        Ant queen = ant(id);
        vector<int> reserves = queen.reserve;
        
        switch (type) {
            case Soldier:
                if (reserves[0] >= soldier_carbo() and reserves[1] >= soldier_prote() and
                    reserves[2] >= soldier_lipid()) {
                    return true;
                }
            case Worker:
                if (reserves[0] >= worker_carbo() and reserves[1] >= worker_prote() and
                    reserves[2] >= worker_lipid()) {
                    return true;
                }
            default:;
        }
        
        return false;
    }
    
    bool queenIsNear(Pos p) {
        if (pos_ok(p)) {
            vector<Dir> direccions = {Up, Right, Down, Left};
            for (int i = 0; i < 4; ++i) {
                Pos next = p + direccions[i];
                Cell c = cell(next);
                if (c.id != -1 and ant(c.id).player == me() and ant(c.id).type == Queen) return true;
            }
        }
        return false;
    }
    
    int bfs_queeny(int id) {
        vector<Dir> direccions = {Up, Right, Down, Left};

        Ant andy = ant(id);
        vector< vector<bool> > visited(board_rows(), vector<bool>(board_cols(), false)); // Matriu de visitats
        bool found = false;
        queue<Pos> cua;
       
        Pos p1 = andy.pos;
        cua.push(p1);
        visited[p1.i][p1.j] = true;
        
        Dir d;
        while (not cua.empty()) {
            Pos p2 = cua.front();
            cua.pop();

            for (int i = 0; i < 4; ++i) {
                d = direccions[i];
                Pos new_p = p2 + d;
                if (pos_ok(new_p) and not visited[new_p.i][new_p.j] and cell(new_p).type != Water) {
                    visited[new_p.i][new_p.j] = true;
                    cua.push(new_p);
                    if (cell(new_p).bonus != None) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }
    
    int bfs_soldier(int id) {
        vector<Dir> direccions = {Up, Right, Down, Left};
        
        Ant andy = ant(id);
        vector< vector<bool> > visited(board_rows(), vector<bool>(board_cols(), false)); // Matriu de visitats
        bool found = false;
        queue<Pos> cua;
        
        Pos p1 = andy.pos;
        cua.push(p1);
        visited[p1.i][p1.j] = true;
        
        Dir d;
        while (not cua.empty()) {
            Pos p2 = cua.front();
            cua.pop();

            for (int i = 0; i < 4; ++i) {
                d = direccions[i];
                Pos new_p = p2 + d;
                if (pos_ok(new_p) and not visited[new_p.i][new_p.j] and cell(new_p).type != Water) {
                    visited[new_p.i][new_p.j] = true;
                    cua.push(new_p);
                    if (cell(new_p).id != -1 and ant(cell(new_p).id).type != Queen and ant(cell(new_p).id).player != me()) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }
    
    int bfs_worker_food(int id) {
        vector<Dir> direccions = {Up, Right, Down, Left};
        
        Ant andy = ant(id);
        vector< vector<bool> > visited(board_rows(), vector<bool>(board_cols(), false)); // Matriu de visitats
        bool found = false;
        queue<Pos> cua;
        
        Pos p1 = andy.pos;
        cua.push(p1);
        visited[p1.i][p1.j] = true;
        
        Dir d;
        while (not cua.empty()) {
            Pos p2 = cua.front();
            cua.pop();

            for (int i = 0; i < 4; ++i) {
                d = direccions[i];
                Pos new_p = p2 + d;
                if (pos_ok(new_p) and not visited[new_p.i][new_p.j] and cell(new_p).type != Water) {
                    visited[new_p.i][new_p.j] = true;
                    cua.push(new_p);
                        
                    if (cell(new_p).bonus != None and not queenIsNear(new_p)) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }
    
    int bfs_worker_queeny(int id, int queenyID) {
        vector<Dir> direccions = {Up, Right, Down, Left};
        
        Ant andy = ant(id);
        vector< vector<bool> > visited(board_rows(), vector<bool>(board_cols(), false)); // Matriu de visitats
        bool found = false;
        queue<Pos> cua;
        
        Pos p1 = andy.pos;
        cua.push(p1);
        visited[p1.i][p1.j] = true;
        
        Dir d;
        while (not cua.empty()) {
            Pos p2 = cua.front();
            cua.pop();
            
            
            for (int i = 0; i < 4; ++i) {
                d = direccions[i];
                Pos new_p = p2 + d;
                if (pos_ok(new_p) and not visited[new_p.i][new_p.j] and cell(new_p).type != Water) {
                    visited[new_p.i][new_p.j] = true;
                    cua.push(new_p);
                    if (cell(new_p).id == queenyID) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }
    
    /**
     * Play method, invoked once per each round.
     */
    virtual void play () {
        vector<Dir> direccions {Up, Right, Down, Left};
        vector<pair<int, Pos>> posiciones;
        
        vector<int> my_queens = queens(me());
        vector<int> my_soldiers = soldiers(me());
        vector<int> my_workers = workers(me());
        
        if (round() % queen_period() == 0) {
            int queenID = my_queens[0];
            int index_q = bfs_queeny(queenID);
            if (index_q != -1) {
                Dir dq = direccions[index_q];
                
                Ant dany = ant(queenID);
                Pos dany_p = dany.pos;
                
                bool found = false;
                Pos next;
                for (int i = 0; i < 4 and not found; ++i) {
                    next = dany_p + dq;
                    if (pos_ok(next) and cell(next).type != Water and
                        (cell(next).id == -1 or (cell(next).id != -1 and
                                                 (not theAntIsMine(cell(next).id, next, posiciones))))) {
                        if (my_soldiers.size() <= (num_ini_soldiers() - 5) and hasReserves(queenID, Soldier)) {
                            bool layed = false;
                            for (int j = 0; j < 4 and not layed; ++j) {
                                Pos lay_next = dany_p + direccions[j];
                                if (pos_ok(lay_next) and cell(lay_next).id == -1) {
                                    lay(queenID, direccions[j], Soldier);
                                    posiciones.push_back(make_pair(queenID, dany_p));
                                    layed = true;
                                }
                            }
                        } else if (my_soldiers.size() <= (num_ini_soldiers() - 5) and hasReserves(queenID, Soldier)) {
                            bool layed = false;
                            for (int j = 0; j < 4 and not layed; ++j) {
                                Pos lay_next = dany_p + direccions[j];
                                if (pos_ok(lay_next) and cell(lay_next).id == -1) {
                                    lay(queenID, direccions[j], Worker);
                                    layed = true;
                                    posiciones.push_back(make_pair(queenID, dany_p));
                                }
                            }
                        } else {
                            move(queenID, dq);
                            posiciones.push_back(make_pair(queenID, dany_p + dq));
                        }
                    } else {
                        dq = direccions[i];
                    }
                }
            }
        }
        
        vector<int> soldiersR = random_permutation(my_soldiers.size());
        for (int j = 0; j < my_soldiers.size(); ++j) {
            int soldierID = my_soldiers[soldiersR[j]];
            int index_s = bfs_soldier(soldierID);
            if (index_s != -1) {
                Dir ds = direccions[index_s];
                
                Ant capi = ant(soldierID);
                Pos capi_p = capi.pos;
                
                bool found = false;
                Pos next;
                for (int i = 0; i < 4 and not found; ++i) {
                    next = capi_p + ds;
                    if (pos_ok(next) and cell(next).type != Water and
                        (cell(next).id == -1 or (cell(next).id != -1 and
                                                 (not theAntIsMine(cell(next).id, next, posiciones) and ant(cell(next).id).type != Queen)))) {
                        move(soldierID, ds);
                        posiciones.push_back(make_pair(soldierID, capi_p + ds));
                        found = true;
                    } else {
                        ds = direccions[i];
                    }
                }
            }
        }
        
        vector<int> workersR = random_permutation(my_workers.size());
        for (int k = 0; k < my_workers.size(); ++k) {
            int workerID = my_workers[workersR[k]];
            Ant minion = ant(workerID);
            int index_w;
            if (minion.bonus == None) {
               index_w = bfs_worker_food(workerID);
            } else {
                index_w = bfs_worker_queeny(workerID, my_queens[0]);
            }
            
            if (index_w != -1) {
                Dir dw = direccions[index_w];
                
                Pos minion_p = minion.pos;
                
                bool found = false;
                Pos next;
                for (int i = 0; i < 4 and not found; ++i) {
                    next = minion_p + dw;
                    if (pos_ok(next) and cell(next).type != Water and
                        (cell(next).id == -1 or (cell(next).id != -1 and
                                                 (not theAntIsMine(cell(next).id, next, posiciones) and (ant(cell(next).id).type != Queen or ant(cell(next).id).type != Soldier))))) {
                        if (cell(minion_p).bonus != None and minion.bonus == None and
                            (not hasReserves(my_queens[0], Soldier) or not hasReserves(my_queens[0], Worker)) and minion.life > worker_life() - 5) {
                            take(workerID);
                            posiciones.push_back(make_pair(workerID, minion_p));
                        } else if (cell(minion_p).bonus == None and minion.bonus != None and
                                   queenIsNear(minion_p)) {
                            leave(workerID);
                            posiciones.push_back(make_pair(workerID, minion_p));
                        } else {
                            move(workerID, dw);
                            posiciones.push_back(make_pair(workerID, minion_p + dw));
                        }
                        found = true;
                    } else {
                        dw = direccions[i];
                    }
                }
            }
        }
    }
};


/**
 * Do not modify the following line.
 */
RegisterPlayer(PLAYER_NAME);
