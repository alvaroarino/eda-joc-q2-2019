#include "Player.hh"


/**
 * Write the name of your player and save this file
 * with the same name and .cc extension.
 */
#define PLAYER_NAME Andy


struct PLAYER_NAME : public Player {
    
    /**
     * Factory: returns a new instance of this class.
     * Do not modify this function.
     */
    static Player* factory () {
        return new PLAYER_NAME;
    }
    
    /**
     * Types and attributes for your player can be defined here.
     */
    
    vector<int> my_queens;
    vector<int> my_soldiers;
    vector<int> my_workers;
    
    vector<Dir> direccions; // Direccions posibles
    
    bool isMyAnt(Pos p, vector< pair<int, Pos> >& posiciones) {
        if (round() == 0) {
            return true;
        } else {
            if (pos_ok(p) and cell(p).type != Water and cell(p).id != -1) {
                for (int i = 0; i < posiciones.size(); ++i) {
                    if (cell(p).id == posiciones[i].first and p == posiciones[i].second) return true;
                }
            }
        }
        return false;
    }
    
    bool hasReserves(int id, AntType type) {
        if (id != -1) {
            Ant dany = ant(id);
            if (type == Soldier) {
                return ((dany.reserve[0] != 0) and (dany.reserve[1] != 0) and (dany.reserve[2] != 0));
            } else {
                return ((dany.reserve[0] != 0) or (dany.reserve[1] != 0) or (dany.reserve[2] != 0));
            }
        }
        return false;
    }
    
    void bfs_queeny(int id, vector< pair<int, Pos> >& posiciones) {
        // Busqueda de huecos
        Ant andy = ant(id);
        vector< vector<bool> > visited(board_rows(), vector<bool>(board_cols(), false)); // Matriu de visitats
        bool found = false;
        queue<Pos> cua;
        
        Pos p1 = andy.pos;
        cua.push(p1);
        visited[p1.i][p1.j] = true;
        Dir d1;
        while (not found and not cua.empty()) {
            Pos p2 = cua.front();
            cua.pop();
            for (int i = 0; i < 4 and not found; ++i) {
                Pos new_p = p2 + direccions[i];
                if (pos_ok(new_p) and not visited[new_p.i][new_p.j] and cell(new_p).type != Water) {
                    visited[new_p.i][new_p.j] = true;
                    cua.push(new_p);
                    if (cell(new_p).id == -1 or (cell(new_p).id != -1 and ant(cell(new_p).id).player != me())) {
                        d1 = direccions[i];
                        
                        bool correct = false;
                        for (int j = 0; j < 4 and not correct; ++j) {
                            Pos next_p = p1 + d1 + direccions[j];
                            if ((cell(next_p).id != -1 and ant(cell(next_p).id).player == me()) or
                                cell(next_p).type == Water or cell(next_p).bonus == None) {
                                if (d1 == Up) d1 = Right;
                                else if (d1 == Right) d1 = Down;
                                else if (d1 == Down) d1 = Left;
                                else d1 = Up;
                            } else {
                                correct = true;
                            }
                        }
                        
                        found = true;
                    }
                }
            }
        }
        
        if (my_soldiers.size() < 10 and hasReserves(id, Soldier)) lay(id, d1, Soldier);
        else if (my_workers.size() < 10 and hasReserves(id, Worker)) lay(id, d1, Worker);
        else move(id, d1);
        posiciones.push_back(make_pair(id, ant(id).pos + d1));
    }
    
    void bfs_soldier(int id, vector< pair<int, Pos> >& posiciones) {
        // Busqueda de hormigas enemiga
        Ant andy = ant(id);
        vector< vector<bool> > visited(board_rows(), vector<bool>(board_cols(), false)); // Matriu de visitats
        bool found = false;
        queue<Pos> cua;
        
        Pos p1 = andy.pos;
        cua.push(p1);
        visited[p1.i][p1.j] = true;
        Dir d1;
        while (not found and not cua.empty()) {
            Pos p2 = cua.front();
            cua.pop();
            for (int i = 0; i < 4 and not found; ++i) {
                Pos new_p = p2 + direccions[i];
                if (pos_ok(new_p) and not visited[new_p.i][new_p.j] and cell(new_p).type != Water) {
                    visited[new_p.i][new_p.j] = true;
                    cua.push(new_p);
                    if (cell(new_p).id == -1 or (cell(new_p).id != -1 and ant(cell(new_p).id).player != me())) {
                        d1 = direccions[i];
                        
                        bool correct = false;
                        for (int j = 0; j < 4 and not correct; ++j) {
                            Pos next_p = p2 + d1 + direccions[j];
                            if (pos_ok(next_p) or (cell(next_p).id != -1 and ant(cell(next_p).id).player == me())) {
                                if (d1 == Up) d1 = Right;
                                else if (d1 == Right) d1 = Down;
                                else if (d1 == Down) d1 = Left;
                                else d1 = Up;
                            } else {
                                correct = true;
                            }
                        }
                        
                        found = true;
                    }
                }
            }
        }
        
        move(id, d1);
        posiciones.push_back(make_pair(id, ant(id).pos + d1));
    }
    
    void bfs_worker_menjar(int id, vector< pair<int, Pos> >& posiciones) {
        Ant andy = ant(id);
        vector< vector<bool> > visited(board_rows(), vector<bool>(board_cols(), false)); // Matriu de visitats
        bool found = false;
        queue<Pos> cua;
        
        Pos p1 = andy.pos;
        cua.push(p1);
        visited[p1.i][p1.j] = true;
        Dir d1;
        while (not found and not cua.empty()) {
            Pos p2 = cua.front();
            cua.pop();
            for (int i = 0; i < 4 and not found; ++i) {
                Pos new_p = p2 + direccions[i];
                if (pos_ok(new_p) and not visited[new_p.i][new_p.j] and cell(new_p).type != Water) {
                    visited[new_p.i][new_p.j] = true;
                    cua.push(new_p);
                    if (cell(new_p).bonus != None or cell(new_p).id == -1) {
                        d1 = direccions[i];
                        
                        bool correct = false;
                        for (int j = 0; j < 4 and not correct; ++j) {
                            Pos next_p = p2 + d1 + direccions[j];
                            if (pos_ok(next_p) or (cell(next_p).id != -1 and ant(cell(next_p).id).player == me())) {
                                if (d1 == Up) d1 = Right;
                                else if (d1 == Right) d1 = Down;
                                else if (d1 == Down) d1 = Left;
                                else d1 = Up;
                            } else {
                                correct = true;
                            }
                        }
                        
                        found = true;
                    }
                }
            }
        }
        
        move(id, d1);
        posiciones.push_back(make_pair(id, ant(id).pos + d1));
    }
    
    void bfs_worker_queeny(int id, vector< pair<int, Pos> >& posiciones) {
        Ant andy = ant(id);
        vector< vector<bool> > visited(board_rows(), vector<bool>(board_cols(), false)); // Matriu de visitats
        bool found = false;
        queue<Pos> cua;
        
        Pos p1 = andy.pos;
        cua.push(p1);
        visited[p1.i][p1.j] = true;
        Dir d1;
        while (not found and not cua.empty()) {
            Pos p2 = cua.front();
            cua.pop();
            for (int i = 0; i < 4 and not found; ++i) {
                Pos new_p = p2 + direccions[i];
                if (pos_ok(new_p) and not visited[new_p.i][new_p.j] and cell(new_p).type != Water) {
                    visited[new_p.i][new_p.j] = true;
                    cua.push(new_p);
                    if (cell(new_p).id == my_queens[0] and ant(cell(new_p).id).player == me()) {
                        d1 = direccions[i];
                        bool correct = false;
                        for (int j = 0; j < 4 and not correct; ++j) {
                            Pos next_p = p2 + d1 + direccions[j];
                            if (not pos_ok(next_p) or
                                (cell(next_p).id != -1 and (ant(cell(next_p).id).player == me() or ant(cell(next_p).id).type != Queen)) or
                                cell(next_p).type == Water) {
                                if (d1 == Up) d1 = Right;
                                else if (d1 == Right) d1 = Down;
                                else if (d1 == Down) d1 = Left;
                                else d1 = Up;
                            } else {
                                correct = true;
                            }
                        }
                        
                        found = true;
                    }
                }
            }
        }
        
        if (cell(p1 + d1).id == my_queens[0]) {
            leave(id);
            posiciones.push_back(make_pair(id, ant(id).pos));
        }
        else {
            move(id, d1);
            posiciones.push_back(make_pair(id, ant(id).pos + d1));
        }
    }
    
    
    /**
     * Play method, invoked once per each round.
     */
    virtual void play () {
        if (round() == 0) {
            direccions = vector<Dir> (4);
            direccions[0] = Up; direccions[1] = Right;
            direccions[2] = Down; direccions[3] = Left;
        }
        
        vector< pair<int, Pos> > posiciones; //id,pos
        my_queens = queens(me());       // La reina
        int queeny_id = my_queens[0];
        if (round() % queen_period() == 0) bfs_queeny(queeny_id, posiciones);
        
        
        my_soldiers = soldiers(me());   // Els soldats
        vector<int> my_soldiers_R = random_permutation(my_soldiers.size());
        for (int i = 0; i < my_soldiers_R.size(); ++i) {
            int id = my_soldiers[my_soldiers_R[i]];
            bfs_soldier(id, posiciones);
        }
        
        my_workers = workers(me());     // Els treballadors
        vector<int> my_workers_R = random_permutation(my_workers.size());
        for (int j = 0; j < my_workers_R.size(); ++j) {
            int id = my_workers[my_workers_R[j]];
            Ant andy = ant(id);
            if (cell(andy.pos).bonus != None) take(id);
            else if (andy.life == 1 and andy.bonus != None) leave(id);
            else if (ant(id).bonus == None) bfs_worker_menjar(id, posiciones);
            else bfs_worker_queeny(id, posiciones);
        }
    }
    
};


/**
 * Do not modify the following line.
 */
RegisterPlayer(PLAYER_NAME);
