#include "Player.hh"


/**
 * Write the name of your player and save this file
 * with the same name and .cc extension.
 */
#define PLAYER_NAME Andy


struct PLAYER_NAME : public Player {
    
    /**
     * Factory: returns a new instance of this class.
     * Do not modify this function.
     */
    static Player* factory () {
        return new PLAYER_NAME;
    }
    
    /**
     * Types and attributes for your player can be defined here.
     */
    
    vector<int> my_queens;
    vector<int> my_soldiers;
    vector<int> my_workers;
    
    vector<Dir> direccions; // Direccions posibles
    
    double distancia(Pos p1, Pos p2) {
        return sqrt(pow(p2.i - p1.i, 2) + pow(p2.j - p1.j, 2));
    }
    
    bool hasReserves(int id, AntType type) {
        if (id != -1) {
            Ant dany = ant(id);
            if (type == Soldier) {
                return ((dany.reserve[0] >= soldier_carbo()) and (dany.reserve[1] >= soldier_prote()) and (dany.reserve[2] >= soldier_lipid()));
            } else {
                return ((dany.reserve[0] >= worker_carbo()) and (dany.reserve[1] >= worker_prote()) and (dany.reserve[2] >= worker_lipid()));
            }
        }
        return false;
    }
    
    bool isTheQueenNear(Pos p) {
        if (cell(p).id == my_queens[0]) return true;
        for (int i = 0; i < 4; ++i) {
            Pos next = p + direccions[i];
            if (pos_ok(next) and cell(next).id == my_queens[0] and
                (not hasReserves(my_queens[0], Soldier) or not hasReserves(my_queens[0], Worker))) {
                return true;
            }
        }
        return false;
    }
    
    bool isTheWorkerNear(Pos p) {
        for (int i = 0; i < 4; ++i) {
            Pos next = p + direccions[i];
            if (pos_ok(next) and cell(next).id != -1 and ant(cell(next).id).player == me() and ant(cell(next).id).type == Worker and ant(cell(next).id).bonus != None) {
                return true;
            }
        }
        return false;
    }
    
    Dir bfs_queeny(int id) {
        // Busqueda de huecos
        Ant andy = ant(id);
        vector< vector<bool> > visited(board_rows(), vector<bool>(board_cols(), false)); // Matriu de visitats
        bool found = false;
        queue<Pos> cua;
        
        Pos p1 = andy.pos;
        cua.push(p1);
        visited[p1.i][p1.j] = true;
        while (not cua.empty()) {
            Pos p2 = cua.front();
            cua.pop();
            for (int i = 0; i < 4; ++i) {
                Pos new_p = p2 + direccions[i];
                if (pos_ok(new_p) and not visited[new_p.i][new_p.j] and cell(new_p).type != Water) {
                    visited[new_p.i][new_p.j] = true;
                    cua.push(new_p);
                    if ((cell(new_p).id != -1 and ant(cell(new_p).id).bonus != None)) {
                        return direccions[i];
                    }
                }
            }
        }
        //return direccions[random(0,3)];
    }
    
    Dir bfs_soldier(int id) {
        // Busqueda de hormigas enemiga
        // Busqueda de huecos
        Ant andy = ant(id);
        vector< vector<bool> > visited(board_rows(), vector<bool>(board_cols(), false)); // Matriu de visitats
        bool found = false;
        queue<Pos> cua;
        
        Pos p1 = andy.pos;
        cua.push(p1);
        visited[p1.i][p1.j] = true;
        while (not cua.empty()) {
            Pos p2 = cua.front();
            cua.pop();
            for (int i = 0; i < 4; ++i) {
                Pos new_p = p2 + direccions[i];
                if (pos_ok(new_p) and (not visited[new_p.i][new_p.j]) and cell(new_p).type != Water) {
                    visited[new_p.i][new_p.j] = true;
                    cua.push(new_p);
                    if (cell(new_p).id != -1 and ant(cell(new_p).id).player != me() and
                        ant(cell(new_p).id).type != Queen) {
                        return direccions[i];
                    }
                }
            }
        }
        //return direccions[random(0,3)];
    }
    
    Dir bfs_worker_menjar(int id) {
        Ant andy = ant(id);
        vector< vector<bool> > visited(board_rows(), vector<bool>(board_cols(), false)); // Matriu de visitats
        bool found = false;
        queue<Pos> cua;
        
        Pos p1 = andy.pos;
        cua.push(p1);
        visited[p1.i][p1.j] = true;
        while (not found and not cua.empty()) {
            Pos p2 = cua.front();
            cua.pop();
            for (int i = 0; i < 4; ++i) {
                Pos new_p = p2 + direccions[i];
                if (pos_ok(new_p) and not visited[new_p.i][new_p.j] and cell(new_p).type != Water) {
                    visited[new_p.i][new_p.j] = true;
                    cua.push(new_p);
                    if (cell(new_p).id == -1 and cell(new_p).bonus != None and not isTheWorkerNear(new_p)) {
                        return direccions[i];
                    }
                }
            }
        }
        //return direccions[random(0,3)];
    }
    
    Dir bfs_worker_queeny(int id) {
        Ant andy = ant(id);
        vector< vector<bool> > visited(board_rows(), vector<bool>(board_cols(), false)); // Matriu de visitats
        bool found = false;
        queue<Pos> cua;
        
        Pos p1 = andy.pos;
        cua.push(p1);
        visited[p1.i][p1.j] = true;
        while (not cua.empty()) {
            Pos p2 = cua.front();
            cua.pop();
            for (int i = 0; i < 4; ++i) {
                Pos new_p = p2 + direccions[i];
                if (pos_ok(new_p) and not visited[new_p.i][new_p.j] and cell(new_p).type != Water) {
                    visited[new_p.i][new_p.j] = true;
                    cua.push(new_p);
                    if (cell(new_p).id != -1 and cell(new_p).id == my_queens[0]) {
                        return direccions[i];
                    }
                }
            }
        }
        //return direccions[random(0,3)];
    }
    
    /**
     * Play method, invoked once per each round.
     */
    virtual void play () {
        if (round() == 0) {
            direccions = vector<Dir> (4);
            direccions[0] = Up; direccions[1] = Right;
            direccions[2] = Down; direccions[3] = Left;
        }

        my_queens = queens(me());       // La reina
        my_soldiers = soldiers(me());   // Els soldats
        my_workers = workers(me());     // Els treballadors
        
        /*
        int queeny_id = my_queens[0];
        if (round() % queen_period() == 0) {
            Ant dany = ant(queeny_id);
            Pos pq = dany.pos;
            
            Dir dq = bfs_queeny(queeny_id);
            
            bool done = false;
            for (int i = 0; i < 4 and not done; ++i) {
                Pos next = pq + dq;
                if (pos_ok(next) and                // Posicio correcta
                    cell(next).type != Water and    // No es aigua
                    // La hormiga es enemiga o no n'hi ha
                    (cell(next).id == -1 or (cell(next).id != -1 and ant(cell(next).id).player != me()))) {
                    done = true;
                } else {
                   if (direccions[i] != dq) dq = direccions[i];
                }
            }
            
            if (done) {
                if (my_soldiers.size() < (num_ini_soldiers() - 3) and hasReserves(queeny_id, Soldier)) lay(queeny_id, dq, Soldier);
                else if (my_workers.size() < (num_ini_workers() - 3) and hasReserves(queeny_id, Worker)) lay(queeny_id, dq, Worker);
                else move(queeny_id, dq);
            }
        }
        
        vector<int> my_soldiers_R = random_permutation(my_soldiers.size());
        for (int l = 0; l < my_soldiers.size(); ++l) {
            int soldier_id = my_soldiers[my_soldiers_R[l]];
            Ant capi = ant(soldier_id);
            Pos ps = capi.pos;
            
            Dir ds = bfs_soldier(soldier_id);
            
            bool done = false;
            for (int j = 0; j < 4 and not done; ++j) {
                Pos next = ps + ds;
                if (pos_ok(next) and
                    cell(next).type != Water and
                    (cell(next).id == -1 or (cell(next).id != -1 and
                                             ant(cell(next).id).player != me() and
                                             ant(cell(next).id).type != Queen))) {
                    done = true;
                } else {
                    if (direccions[j] != ds) ds = direccions[j];
                }
            }
            
            if (done) {
                move(soldier_id, ds);
            }
        }
         */
        
        vector<int> my_workers_R = random_permutation(my_workers.size());
        for (int j = 0; j < my_workers.size(); ++j) {
            int worker_id = my_workers[my_workers_R[j]];
            Ant minion = ant(worker_id);
            Pos pw = minion.pos;
            
            Dir dw;
            if (minion.bonus == None) dw = bfs_worker_menjar(worker_id);
            else dw = bfs_worker_queeny(worker_id);
            
            bool done = false;
            for (int k = 0; k < 4 and not done; ++k) {
                Pos next = pw + dw;
                if (pos_ok(next) and
                    cell(next).type != Water and
                    (cell(next).id == -1 or (cell(next).id != -1 and
                                             ant(cell(next).id).player != me() and
                                             (ant(cell(next).id).type == Worker)))) {
                    done = true;
                } else {
                    if (direccions[k] != dw) dw = direccions[k];
                }
            }
            
            if (done) {
                move(worker_id, dw);
            }
        }
    }
    
};


/**
 * Do not modify the following line.
 */
RegisterPlayer(PLAYER_NAME);
