#include "Player.hh"


/**
 * Write the name of your player and save this file
 * with the same name and .cc extension.
 */
#define PLAYER_NAME Andy


struct PLAYER_NAME : public Player {
    
    /**
     * Factory: returns a new instance of this class.
     * Do not modify this function.
     */
    static Player* factory () {
        return new PLAYER_NAME;
    }
    
    /**
     * Types and attributes for your player can be defined here.
     */
    
    bool queenNear(Pos p) {
        vector<Dir> direccions = {Up, Right, Down, Left};
        for (int i = 0; i < 4; ++i) {
            Pos next = p + direccions[i];
            if (pos_ok(next) and cell(next).id != -1 and ant(cell(next).id).type != Queen) {
                for (int j = 0; j < 4; ++j) {
                    Pos next_j = next + direccions[j];
                    if (pos_ok(next_j) and cell(next_j).id != -1 and ant(cell(next_j).id).type == Queen) {
                        return true;
                    }
                }
            } else if (pos_ok(next) and cell(next).id != -1 and ant(cell(next).id).type == Queen) {
                return true;
            }
        }
        return false;
    }
    
    bool workerNear(Pos p) {
        vector<Dir> direccions = {Up, Right, Down, Left};
        for (int i = 0; i < 4; ++i) {
            Pos next = p + direccions[i];
            if (pos_ok(next) and cell(next).id != -1 and ant(cell(next).id).type != Worker) {
                for (int j = 0; j < 4; ++j) {
                    Pos next_j = next + direccions[j];
                    if (pos_ok(next_j) and cell(next_j).id != -1 and ant(cell(next_j).id).type == Worker) {
                        return true;
                    }
                }
            } else if (pos_ok(next) and cell(next).id != -1 and ant(cell(next).id).type == Worker) {
                return true;
            }
        }
        return false;
    }
    
    int bfs_queeny(int id) {
        vector<Dir> direccions = {Up, Right, Down, Left};
        // Busqueda de huecos
        Ant andy = ant(id);
        vector< vector<bool> > visited(board_rows(), vector<bool>(board_cols(), false)); // Matriu de visitats
        bool found = false;
        queue<Pos> cua;
        queue<int> first;
        
        first.push(-1);
        
        Pos p1 = andy.pos;
        cua.push(p1);
        visited[p1.i][p1.j] = true;
        while (not cua.empty()) {
            Pos p2 = cua.front();
            cua.pop();
            
            int d_first = first.front();
            first.pop();
            
            for (int i = 0; i < 4; ++i) {
                Pos new_p = p2 + direccions[i];
                if (pos_ok(new_p) and not visited[new_p.i][new_p.j]) {
                    visited[new_p.i][new_p.j] = true;
                    if (cell(new_p).type != Water) {
                        cua.push(new_p);
                        if (d_first == -1) first.push(i);
                        else first.push(d_first);
                        
                        if (cell(new_p).bonus != None) {
                            return first.front();
                        }
                    }
                }
            }
        }
        return first.front();
    }
    
    int bfs_soldier(int id) {
        vector<Dir> direccions = {Up, Right, Down, Left};
        // Busqueda de enemigos
        Ant andy = ant(id);
        vector< vector<bool> > visited(board_rows(), vector<bool>(board_cols(), false)); // Matriu de visitats
        bool found = false;
        queue<Pos> cua;
        queue<int> first;
        
        first.push(-1);
        
        Pos p1 = andy.pos;
        cua.push(p1);
        visited[p1.i][p1.j] = true;
        while (not cua.empty()) {
            Pos p2 = cua.front();
            cua.pop();
            
            int d_first = first.front();
            first.pop();
            
            for (int i = 0; i < 4; ++i) {
                Pos new_p = p2 + direccions[i];
                if (pos_ok(new_p) and not visited[new_p.i][new_p.j]) {
                    visited[new_p.i][new_p.j] = true;
                    if (cell(new_p).type != Water) {
                        cua.push(new_p);
                        if (d_first == -1) first.push(i);
                        else first.push(d_first);
                        
                        if (cell(new_p).id != -1 and ant(cell(new_p).id).player != me() and ant(cell(new_p).id).type != Queen) {
                            return first.front();
                        }
                    }
                }
            }
        }
        return first.front();
    }
    
    int bfs_worker_food(int id) {
        vector<Dir> direccions = {Up, Right, Down, Left};
        // Busqueda de enemigos
        Ant andy = ant(id);
        vector< vector<bool> > visited(board_rows(), vector<bool>(board_cols(), false)); // Matriu de visitats
        bool found = false;
        queue<Pos> cua;
        queue<int> first;
        
        first.push(-1);
        
        Pos p1 = andy.pos;
        cua.push(p1);
        visited[p1.i][p1.j] = true;
        while (not cua.empty()) {
            Pos p2 = cua.front();
            cua.pop();
            
            int d_first = first.front();
            first.pop();
            
            for (int i = 0; i < 4; ++i) {
                Pos new_p = p2 + direccions[i];
                if (pos_ok(new_p) and not visited[new_p.i][new_p.j]) {
                    visited[new_p.i][new_p.j] = true;
                    if (cell(new_p).type != Water) {
                        cua.push(new_p);
                        if (d_first == -1) first.push(i);
                        else first.push(d_first);
                        
                        if (cell(new_p).bonus != None or
                            not (queenNear(new_p) or workerNear(new_p))) {
                            return first.front();
                        }
                    }
                }
            }
        }
        return first.front();
    }
    
    /**
     * Play method, invoked once per each round.
     */
    virtual void play () {
        vector<Dir> direccions {Up, Right, Down, Left};
        
        vector<int> my_queens = queens(me());
        if (round() % queen_period() == 0) {
            int queenID = my_queens[0];
            int dany_bfs = bfs_queeny(queenID);
            if (dany_bfs != -1) {
                Ant dany = ant(queenID);
                Pos dany_next = dany.pos + direccions[dany_bfs];
                
                if (pos_ok(dany_next) and cell(dany_next).type != Water and cell(dany_next).id == -1) {
                    move(queenID, direccions[dany_bfs]);
                }
            }
        }
        
        vector<int> my_soldiers = soldiers(me());
        vector<int> soldiersR = random_permutation(my_soldiers.size());
        for (int j = 0; j < my_soldiers.size(); ++j) {
            int soldierID = my_soldiers[soldiersR[j]];
            int soldier_bfs = bfs_soldier(soldierID);
            if (soldier_bfs != -1) {
                Ant capi = ant(soldierID);
                Pos capi_next = capi.pos + direccions[soldier_bfs];
                
                if (pos_ok(capi_next) and cell(capi_next).type != Water and
                    cell(capi_next).id == -1) {
                    move(soldierID, direccions[soldier_bfs]);
                }
            }
        }
        
        vector<int> my_workers = workers(me());
        vector<int> workersR = random_permutation(my_workers.size());
        for (int k = 0; k < my_workers.size(); ++k) {
            int workerID = my_workers[k];
            int worker_bfs = bfs_worker_food(workerID);
            if (worker_bfs != -1) {
                Ant minion = ant(workerID);
                Pos minion_next = minion.pos + direccions[worker_bfs];
                
                if (pos_ok(minion_next) and cell(minion_next).type != Water and
                    cell(minion_next).id == -1) {
                    move(workerID, direccions[worker_bfs]);
                }
            }
        }
    }
};


/**
 * Do not modify the following line.
 */
RegisterPlayer(PLAYER_NAME);
